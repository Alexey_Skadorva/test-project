package test.project.client;

import java.util.List;

import test.project.shared.FieldVerifier;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Test_project implements EntryPoint {

	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private final String BUTTON_NAME = "Process";
	private final String EMPTY_BOX_MESSAGE= "Please, enter value";
	private final String SERVER_ERROR_MESSAGE= "Error in server";
	private final String STYLE_NAME_OF_TEXT_AREA = "nameField";

	private final int TEXT_AREA_SIZE = 300;

	public void onModuleLoad() {
		
		final Button sendButton = new Button(BUTTON_NAME);
		final TextArea nameField = new TextArea();
		nameField.setName(STYLE_NAME_OF_TEXT_AREA);
		nameField.setPixelSize(TEXT_AREA_SIZE, TEXT_AREA_SIZE);
		final Label errorLabel = new Label();
		
		sendButton.addStyleName("sendButton");

		RootPanel.get("nameFieldContainer").add(nameField);
		RootPanel.get("sendButtonContainer").add(sendButton);
		RootPanel.get("errorLabelContainer").add(errorLabel);
		nameField.setFocus(true);
		nameField.selectAll();

		sendButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sendButton.setEnabled(true);
				sendButton.setFocus(true);
			}
		});

		class MyHandler implements ClickHandler, KeyUpHandler {
		
			public void onClick(ClickEvent event) {
				sendNameToServer();
			}

			public void onKeyUp(KeyUpEvent event) {
				
			}

			private void sendNameToServer() {
				errorLabel.setText("");
				String textToServer = nameField.getText();
				if (!FieldVerifier.isValidName(textToServer)) {
					errorLabel.setText(EMPTY_BOX_MESSAGE);
					return;
				}
				
				greetingService.greetServer(textToServer,
						new AsyncCallback< List<List<String>>>() {
							public void onFailure(Throwable caught) {
								errorLabel.setText(SERVER_ERROR_MESSAGE);
							}

							public void onSuccess(List<List<String>> result) {
								RootPanel.get("grid").clear();
							    CellTable<List<String>> table = new CellTable<List<String>>();
							    for(int i = 0; i < result.get(0).size();i++){
							    	final int j=i;
							    	TextColumn<List<String>> nameColumn = new TextColumn<List<String>>() {
								         @Override
								         public String getValue(List<String> line) {
								            return line.get(j);
								         }
								    };
							        table.addColumn(nameColumn, i+1+"");						
							    }
							  
							    table.setRowCount(result.size(), true);

							    table.setRowData(0,result);
							    VerticalPanel panel = new VerticalPanel();
							    panel.setBorderWidth(1);	    
							    panel.add(table);
								RootPanel.get("grid").add(table);
							    RootPanel.get().add(panel);
							}
					});
			}
		}
		MyHandler handler = new MyHandler();
		sendButton.addClickHandler(handler);
		nameField.addKeyUpHandler(handler);
	}
}
