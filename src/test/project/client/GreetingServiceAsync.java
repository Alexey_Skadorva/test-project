package test.project.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GreetingServiceAsync {
	void greetServer(String name,
			AsyncCallback<List<List<String>>> asyncCallback);
}
