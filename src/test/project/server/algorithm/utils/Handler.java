package test.project.server.algorithm.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Handler {
	
	private static String NUMBERS_PATTERN = "-?\\d+(\\.\\d+)?";
	
	/*
	 * This method splits a string to whitespace and converts string to list.
	 */
	public static List<List<String>> parseInputStringToList(String input){
		String[] mass = input.split("\n");
		List<List<String>> list = new ArrayList<>();
		
		for(String elem : mass){
			list.add(new ArrayList<String>(Arrays.asList(elem.replaceAll("( )+", " ").split(" "))));
		}
		
		return list;
	}
	
	/*
	 * This method align list. Rows are added to blank values to all
	 *  the lines become equal.
	 */
	public static void alignRows(List<List<String>> list){
		 int lengthOfBigestRow = list.get(0).size();
		 for(List<String> next : list){
			 if(lengthOfBigestRow < next.size()){
				 lengthOfBigestRow = next.size();
			 }
		 }

		 for(List<String> next : list){
		    	 if(lengthOfBigestRow > next.size()){
		    		 for(int i = next.size();i < lengthOfBigestRow;i++){
		    			 if(next != null)
		    			 next.add(next.size()," ");
		    		 }
		    	 }
	      }
	}
	
	/*
	 * This method sorting column.
	 */
	public static void sortingColumn(List<List<String>> list, int firstInSort, int secondInSort, int column){
		Pattern p = Pattern.compile(NUMBERS_PATTERN);
		list.subList(firstInSort, secondInSort).sort(new Comparator<List<String>>(){
            public int compare(List<String> row1, List<String> row2)
            {  	    
            	Matcher m1 = p.matcher(row1.get(column));
                Matcher m2 = p.matcher(row2.get(column));

            	if(m1.matches() && m2.matches()){
        			return new Integer(row1.get(column)).compareTo(new Integer(row2.get(column)));
            	}else if(m1.matches()){
            		return -1;
            	}else if(m2.matches()){
            		return 1;
            	}else{
            		return row1.get(column).compareTo(row2.get(column));
            	}
            }
		});
	}
	
	/*
	 * This method defines whether there is a repetition of the list.
	 */
	public static boolean checkColumnToDuplicates(List<String> column){
        Set<String> set = new HashSet<>(column);
        if(set.size() < column.size()) {
        	return true;
        }
        return false;
	}
}
