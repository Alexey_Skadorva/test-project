package test.project.server.algorithm;

import java.util.ArrayList;
import java.util.List;
import test.project.server.algorithm.utils.Handler;


public class Algorithm {
	List<List<String>> list;
	
	public List<List<String>> calculate(String input){
		list = Handler.parseInputStringToList(input);
		Handler.alignRows(list);
		sortingColumn(0,list.size(),0);
		return list;
	}
	
	void sortingColumn(int firstInSort, int secondInSort, int column){

        Handler.sortingColumn(list, firstInSort, secondInSort, column);

        List<String> currentColumn = new ArrayList<>();
        
        for(int i = firstInSort;i < secondInSort;i++){
            currentColumn.add(list.get(i).get(column));
        }
        
        if(Handler.checkColumnToDuplicates(currentColumn)) {
            int firstDuplicate = 0;
            int lastDuplicate = 0;
            for (int i = 0; i < secondInSort - firstInSort-1; i++) {
                if(currentColumn.get(i).equals(currentColumn.get(i+1))){
                    if(lastDuplicate == 0){
                    	firstDuplicate = firstInSort+i;
                    }
                    lastDuplicate = firstInSort + i + 1;
                }else{
                    if(lastDuplicate != 0 && column + 1 != list.get(0).size()){
                    	sortingColumn(firstDuplicate, lastDuplicate + 1, column + 1);
                    }
                    firstDuplicate = 0;
                    lastDuplicate = 0;
                }
            }
            
            if(lastDuplicate != 0 && column + 1 != list.get(0).size()){
            	sortingColumn(firstDuplicate, lastDuplicate + 1, column + 1);
                firstDuplicate = 0;
                lastDuplicate = 0;
            }
        }
    }
}
