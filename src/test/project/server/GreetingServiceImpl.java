package test.project.server;

import java.util.List;

import test.project.server.algorithm.Algorithm;
import test.project.client.GreetingService;
import test.project.shared.FieldVerifier;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
	Algorithm algorithm = new Algorithm();

	public List<List<String>> greetServer(String input) throws IllegalArgumentException {
		input = escapeHtml(input);
		return algorithm.calculate(input);
	}

	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}

	public List<List<String>> greetServer(List<List<String>> name) {
		return null;
	}
}
